April 2008 (updated)

The SIL Encore IPA and SIL IPA93 fonts are obsolete, symbol-encoded fonts. 
Their use is discouraged. If you decide to download and use these fonts, 
please note there is no user support for these fonts.

Documentation files are located at C:\Program Files\SIL\Fonts\SILIPA or on the 
Start menu at Start / All Programs / SIL / Fonts / SILIPA. Not all of the 
documentation files from the original package are included in the current
package as they are in a format incompatible with modern computers AND
because we expect people to only use these fonts for existing documents. If
you are creating new documents you should be using the 
Doulos SIL (https://scripts.sil.org/DoulosSILfont) or 
Charis SIL (https://scripts.sil.org/CharisSILfont) fonts which provide complete
IPA AND Unicode support.


April 1995

Dear Linguist,

The enclosed files constitute the SIL Encore IPA Fonts. They are
provided in TrueType format only. Please read the accompanying 
documentation for more specific information on the fonts.

SIL International is an organization of linguists dedicated to the 
study and promotion of the thousands of minority languages around 
the world. International Publishing Services serves SIL by developing 
products to assist in the publication of linguistic texts.

We are happy to make these IPA fonts available to the general
linguistic community at no charge. Please note that these fonts do 
not provide Unicode support. Their use is discouraged. The 
Doulos SIL (https://scripts.sil.org/DoulosSILfont) and 
Charis SIL (https://scripts.sil.org/CharisSILfont) fonts do provide 
complete Unicode IPA support. However, you must convert existing data using 
the "SIL IPA93 Fonts" to Unicode before using the Doulos SIL or 
Charis SIL font. 

You may share these fonts with your friends
and co-workers, but with the following restrictions:

	* All files must be copied together, including this one.
	* No fee may be charged for the fonts

Even though these fonts are "free", SIL retains all copyright and
ownership of the included fonts and reserves the right to restrict
their use and distribution at any time. They may not be used as a
basis for other fonts. They may be placed on bulletin boards, etc.
but cannot be included in (or bundled with) any commercial product
including hard disks or CD-ROMs without specific written permission.

TECHNICAL SUPPORT

The SIL Encore IPA and SIL IPA93 fonts are obsolete, symbol-encoded fonts. 
Their use is discouraged. If you decide to download and use these fonts, 
please note there is no user support for these fonts.

These fonts are intended for use by experienced computer users. Installing 
and using these fonts is not a trivial matter. The most effective technical 
support is usually provided by an experienced computer user who can 
personally sit down with you at your computer to troubleshoot the problem.

If you are having difficulty, be sure to do the following:

    * Carefully read all the documentation provided with the fonts, 
      including the documentation installed by the installation program.
    * Check out all the links on the web pages for these font packages, 
      and read all the information and instructions they contain. 
    
SIL International
Non-Roman Script Initiative
7500 W. Camp Wisdom Rd.
Dallas, TX 75236
USA
Email:  SIL_fonts@sil.org


          ATTENTION POSTSCRIPT USERS WHO ARE UPGRADING
          FROM VERSION 1.0 (SIL Premier IPA Fonts):

If you have been using the PostScript fonts from an earlier version of the
SIL Premier/Encore IPA Fonts you should be sure to de-install those fonts
(using the ATM Control Panel) before installing the new fonts. If you only
use TrueType, the provided installation program will automatically remove
the older fonts from your system before installing the new ones.


INSTALLING IN WINDOWS

The SIL IPA Fonts 1.21 package automatically installs
the TrueType fonts and documentation when accessed through Microsoft Windows.

While in Explorer select the drive icon where the SILIPA.EXE file 
is located. Find SILIPA.EXE in the directory window and double-click on it.

If you downloaded the fonts from the Internet, then you will select the 
drive:\directory where you placed the file SILIPA.EXE.

Example:  C:\DOWNLOAD\SILIPA.EXE

In this example, the file was placed in the \DOWNLOAD directory on
drive C. (Select the actual drive and directory where you placed your
files.)

If the SILIPA.EXE file is located on a CD

Example:  D:\SILIPA.EXE

then select drive D:. In this example, the fonts are being installed from drive D.

After double-clicking on SILIPA.EXE follow the instructions and respond to the dialog
boxes.

Once the font and documentation are installed you can access the documentation
from Start / All Programs / SIL / Fonts / SILIPA.



          CHANGES FOR VERSION 1.2

***Linking Character***

Character 237 in the original (1.0) IPA fonts was a non-spacing diacritic.
This really should have been a spacing character. Although the IPA calls
that character a "Bottom Tie Bar" (analogous to the Top one in code 131),
it signifies "Linking (absence of a break)" and is used to link two sounds
that are written with a space between them. Hence, these fonts have 237 as
a spacing character.


***Doulos Character Spacing***

After a number of comments from users, we have "relaxed" the spacing of the
SILDoulosIPA font to conform to that of the SILDoulos fonts in the SIL Encore
Fonts. This means that there will be more spacing between the characters,
making the text easier to read. This also means that texts set in the original
SILDoulosIPA will have different line breaks if opened on a computer with the
new SILDoulosIPA. If you have a strong reason to retain the original spacing,
do not install this new font. We do however recommend that you use this new
font.


***Sophia Dieresis***

The three dieresis characters (45, 95, 208) in the SILSophiaIPA font now have
dots spaced further apart.


***Manuscript Space Width***

The space character in the SILManuscriptIPA font is now (reliably) equal in
width to the other characters. 


          CHANGES FOR VERSION 1.21

The KEYMAN utility was added with the keyboard translation file IPA.KMN.