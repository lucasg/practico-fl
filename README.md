Practico-fl
===========

* HomePage: [https://bitbucket.org/lucasg/practico-fl](https://bitbucket.org/lucasg/practico-fl)
* Downloads: [https://bitbucket.org/lucasg/practico-fl/downloads](https://bitbucket.org/lucasg/practico-fl/downloads)


Intro
-----

Scripts for transforming mocovi excel tables to pdf or html documents.
In particular the scripts do the following pipeline:
`xls -> csv -> xml -> pdf/html`

## The main.py script

* Uses all the sub scripts to do all the pipeline in one step or you can stop
  at the xml step using the `-onlyxml` flag.
* Assumes [Apache FOP](http://xmlgraphics.apache.org/fop/) is already
  installed. Binaries can be downloaded directly from:
  [http://xmlgraphics.apache.org/fop/download.html](http://xmlgraphics.apache.org/fop/download.html)
* It can generate html/pdf documents specifying the -tohtml/-pdf respectively.
  (If no flag is specified it generates html files)
* You can specify a output prefix. (By default uses the name of the sheets)
* To execute:

    $ ./main.py <excelfile>

* Help:

    $ ./main.py -h


The fonts
---------

The SILxxIPA fonts are required for proper pdf rendering and html
visualization. Without them is possible that some symbols are not recognized
at rendering time and they will appear as '#' characters or different symbols
in html documents.

The fonts are already included in the fonts/ directory.
To install them double click them if in Ubuntu (in click install) or run as
root:

    # cp fonts/*.ttf /usr/share/fonts
    # fc-cache -fv

A bit more complete instructions can be found in fonts/INSTALL file.


The scripts
-----------

You can see the help for each script using the -h flag. You can also test the
behaviour of the scripts using the files in the samples directory.

In example:

    $ ./scripts/csv2xml.py -h
    $ ./scripts/csv2xml.py samples/texto_1.csv


Example of use
--------------

    $ ./main.py samples/base_datos_PIO-MOC.xls
    $ ls
      ... texto_1.html texto_2.html ... texto_6.html

    or

    $ ./main.py -topdf -o mysheet samples/base_datos_PIO-MOC.xls
    $ ls
      ... mysheet_1.pdf mysheet_2.pdf ... mysheet_6.pdf

    or

    $ ./main.py -onlyxml -o myxml samples/base_datos_PIO-MOC.xls
    $ ls
      ... myxml_1.xml myxml_2.xml ... myxml_6.xml


FAQ
---

#### Why are main.py and/or xls2csv.py not working when I move them to another directory?

The xls2csv.py (which is used by main.py) depends on the `xlrd` third-party
python library. The same one **is** already included in the package but if you
move one of the scripts, mentioned above, they wont be able to tell where
the library is.

To solve this you can install `xlrd` on your system running the following
commands:

    $ apt-get install pip
    $ pip install xlrd

Remember that main.py still needs the `scripts` package to function.
