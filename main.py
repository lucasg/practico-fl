#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Convert all the tables in an excel document to html or pdf documents.
Write the resulting files with the same name as the tables but with
.hmtl or pdf extensions.
"""

import argparse
import itertools
import os
from StringIO import StringIO
from subprocess import Popen, PIPE

from scripts import xls2csv
from scripts import csv2xml

SCRIPTS = {
    'html': 'xml2html.xsl',
    'pdf': 'xml2pdf.xsl',
}
THIS_DIR = os.path.dirname(__file__)
SCRIPTS_DIR = 'scripts'
FOP_CONFIG = os.path.join(THIS_DIR, SCRIPTS_DIR, 'myfop.xconf')

CMD = "fop -c " + FOP_CONFIG + " -xml - -xsl %s %s %s.%s"


def _script(format_):
    return os.path.join(THIS_DIR, SCRIPTS_DIR, SCRIPTS[format_])


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('exceldoc', type=xls2csv._xls_file,
                        help='an xls/xlsx file')
    parser.add_argument('-o', dest='outprefix', metavar='outprefix',
                        help='output file prefix')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-tohtml', action='store_true',
                       help='output to html (default if both omitted)')
    group.add_argument('-topdf', action='store_true', help='output to pdf')
    group.add_argument('-onlyxml', action='store_true',
                       help='output only the xmls')
    args = parser.parse_args()

    counter = itertools.count(start=1, step=1) if args.outprefix else None
    tree = None
    for sheet in xls2csv.sheets(args.exceldoc):
        pipe = StringIO()

        # Write worksheet as csv and return pipe pos to start
        xls2csv.write_csv(sheet, pipe)
        pipe.seek(0)

        # Build output file name
        outfile = (args.outprefix + '_%d' % counter.next()
                   if args.outprefix
                   else sheet.name)

        # Build and write xml tree
        tree = csv2xml.build_tree(pipe)
        pipe2 = StringIO()
        if args.onlyxml:
            outname = ''.join([outfile, '.xml'])
            tree.write(outname, 'utf-8')
            csv2xml.prettify_xml(outname)
            continue
        else:
            tree.write(pipe2, 'utf-8')

        # Build cmd
        scriptname = _script('pdf') if args.topdf else _script('html')
        extension = 'pdf' if args.topdf else 'html'
        outflag = '-pdf' if args.topdf else '-foout'
        cmd = CMD % (scriptname, outflag, outfile, extension)

        # Run fop
        fop = Popen(cmd.split(), stdout=PIPE, stdin=PIPE, stderr=PIPE)
        output, outerr = fop.communicate(pipe2.getvalue())

        if output is not '':
            print output
        if outerr is not '':
            print outerr

        pipe.close()
        pipe2.close()


if __name__ == '__main__':
    main()
