<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    >

  <xsl:template match="tei:teiCorpus/tei:TEI/tei:text/tei:group">
    <html>

      <head>
        <style type="text/css" media="screen, print">
          @font-face {
            font-family: "SILDoulosIPA";
            src: url("sildipa.ttf");
          }
        </style>
      </head>

      <body>
        <table border="1">
          <tr bgcolor="#5aaed4">
            <th>Trad. libre (Esp)</th>
            <th>IPA</th>
            <th>TNR</th>
            <th>MORPH</th>
            <th>GLOSS</th>
          </tr>
          <xsl:for-each select="tei:text">
            <tr>
              <td><xsl:value-of select="tei:body/tei:div[@type='Trad. libre (Esp)']"/></td>
              <td><xsl:value-of select="tei:body/tei:div[@type='IPA']"/></td>
              <td><xsl:value-of select="tei:body/tei:div[@type='TNR']"/></td>
              <td><xsl:value-of select="tei:body/tei:div[@type='MORPH']"/></td>
              <td><xsl:value-of select="tei:body/tei:div[@type='GLOSS']"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>

    </html>
  </xsl:template>

</xsl:stylesheet>
