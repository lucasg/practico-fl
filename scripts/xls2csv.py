#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Convert all the work sheets from an excel document to csv files.
Write the resulting csv files with the same name as the sheets.
"""

import argparse
import csv
import os
import sys
import inspect

# Use included external library for parsing xls/xlsx documents.
path = os.path.abspath(inspect.getsourcefile(lambda _: None))  # this file path
xlrd_path = os.path.join(os.path.dirname(path), 'xlrd')
sys.path.append(xlrd_path)
import xlrd

from udecoder import UnicodeCsvWriter

NEWLINE = '\n'   # Change it to '\r\n' for windows if you have problems


def sheets(excelfilename):
    """Returns a generator with the sheets from excelfile."""
    workbook = xlrd.open_workbook(excelfilename)
    all_worksheets = workbook.sheet_names()
    for name in all_worksheets:
        sheet = workbook.sheet_by_name(name)
        yield sheet


def write_csv(sheet, outfile):
    """Writes a csv file from an xls sheet."""
    writer = UnicodeCsvWriter(outfile, quoting=csv.QUOTE_ALL,
                              lineterminator=NEWLINE)

    for i in xrange(sheet.nrows):
        entries = [x for x in sheet.row_values(i)]
        writer.writerow(entries)


def _xls_file(string):
    ext = string[-4:]
    if ext != '.xls' and ext != '.xlsx':
        msg = "%r is not a xls/xlsx file" % string
        raise argparse.ArgumentTypeError(msg)
    elif not os.path.isfile(string):
        msg = "%r does not exists" % string
        raise argparse.ArgumentTypeError(msg)
    else:
        return string


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('xlsfile', type=_xls_file, help='an xls/xlsx file')
    args = parser.parse_args()

    # Write each worksheet to sheetname.csv
    for sheet in sheets(args.xlsfile):
        outname = ''.join([sheet.name, '.csv'])
        try:
            with open(outname, 'wb') as outfile:
                write_csv(sheet, outfile)
        except IOError:
            print "Couldn't write to %s." % outname


if __name__ == "__main__":
    main()
