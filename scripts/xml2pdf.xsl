<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    >

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple" page-height="29.7cm"
          page-width="21cm" margin-top="1cm" margin-bottom="1cm"
          margin-left="1cm" margin-right="1cm">
          <fo:region-body margin-top="0.5cm" />
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="simple">
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates select="tei:teiCorpus" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="tei:teiCorpus">
    <fo:block>
      <xsl:apply-templates select="tei:TEI" />
    </fo:block>
  </xsl:template>

  <xsl:template match="tei:TEI/tei:text/tei:group">
    <fo:block>
      <fo:table table-layout="fixed" width="100%" background-color="white"
        line-height="16pt" space-before.optimum="10pt"
        space-after.optimum="6pt" text-align="left">
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-header background-color="#5aaed4">
          <fo:table-row>
            <fo:table-cell>
              <fo:block color="#000" font-size="11pt" text-align="center">Trad. libre (Esp)</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#000" font-size="11pt" text-align="center">IPA</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#000" font-size="11pt" text-align="center">TNR</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#000" font-size="11pt" text-align="center">MORPH</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#000" font-size="11pt" text-align="center">GLOSS</fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-header>
        <xsl:for-each select="tei:text">
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#222222" border-bottom-style="groove" color="#555">
                <fo:block padding-bottom="1mm" padding-top="1mm" font-family="SILDoulosIPA" font-style="normal" font-weight="normal" font-size="10pt">
                  <xsl:value-of select="tei:body/tei:div[@type='Trad. libre (Esp)']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#222222" border-bottom-style="groove" color="#555">
                <fo:block padding-bottom="1mm" padding-top="1mm" font-family="SILDoulosIPA" font-style="normal" font-weight="normal" font-size="10pt">
                  <xsl:value-of select="tei:body/tei:div[@type='IPA']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#222222" border-bottom-style="groove" color="#555">
                <fo:block padding-bottom="1mm" padding-top="1mm" font-family="SILDoulosIPA" font-style="normal" font-weight="normal" font-size="10pt">
                  <xsl:value-of select="tei:body/tei:div[@type='TNR']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#222222" border-bottom-style="groove" color="#555">
                <fo:block padding-bottom="1mm" padding-top="1mm" font-family="SILDoulosIPA" font-style="normal" font-weight="normal" font-size="10pt">
                  <xsl:value-of select="tei:body/tei:div[@type='MORPH']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#222222" border-bottom-style="groove" color="#555">
                <fo:block padding-bottom="1mm" padding-top="1mm" font-family="SILDoulosIPA" font-style="normal" font-weight="normal" font-size="10pt">
                  <xsl:value-of select="tei:body/tei:div[@type='GLOSS']" />
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </xsl:for-each>
      </fo:table>
    </fo:block>
  </xsl:template>

</xsl:stylesheet>
