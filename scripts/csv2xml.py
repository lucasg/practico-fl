#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Convert a csv file to an xml one with a predefined structure.
If no 'outfile' is specified it saves to the same name of
the input file ('csvfile') but with .xml extension.
"""

import argparse
import codecs
import os
import xml.dom.minidom as MiniDom
import xml.etree.ElementTree as ET

from udecoder import UnicodeCsvReader

# Predefined column positions.
# In this order they will be the resulting xml.
# Change this if the columns structure differs.
COLUMNS = [
    ('Trad. libre (Esp)', 3),
    ('IPA', 1),
    ('TNR', 2),
    ('MORPH', 5),
    ('GLOSS', 6)
]

# Change this if the namespace changes.
NAMESPACE = 'http://www.tei-c.org/ns/1.0'

# Pretty print
INDENT = "    "  # Using 4-spaces; change it to '\t' to use tabs.
NEWLINE = '\n'   # Change it to '\r\n' for windows if you have problems


def build_tree(csvfile):
    """Build a predefined xml tree."""
    reader = UnicodeCsvReader(csvfile)
    reader.next()  # discard first row

    teicorpus = ET.Element('teiCorpus', {'xmlns': NAMESPACE})  # root elem
    teiheader = ET.SubElement(teicorpus, 'teiHeader')
    teiheader.text = " "
    tei = ET.SubElement(teicorpus, 'TEI')
    text = ET.SubElement(tei, 'text')
    group = ET.SubElement(text, 'group')

    for row in reader:
        textentry = ET.SubElement(group, 'text',
                                  {'xml:id': 'T_1.%d_MOC' % int(float(row[0])),
                                   'xml:lang': 'moc'})

        body = ET.SubElement(textentry, 'body')
        for each in COLUMNS:
            tag = ET.SubElement(body, 'div', {'type': each[0]})
            tag.text = row[each[1]]

    tree = ET.ElementTree(teicorpus)
    return tree


def prettify_xml(xmlfile, indent=INDENT, newl=NEWLINE):
    """Indent and xml file."""
    dom = MiniDom.parse(xmlfile)
    with codecs.open(xmlfile, 'wb', 'utf-8') as f:
        f.write(dom.toprettyxml(indent, newl))


def _csv_file(string):
    if string[-4:] != '.csv':
        msg = "%r is not a .csv file" % string
        raise argparse.ArgumentTypeError(msg)
    else:
        try:
            return open(string, 'rb')
        except IOError:
            msg = "%r does not exists" % string
            raise argparse.ArgumentTypeError(msg)


def _xml_file(string):
    if string[-4:] != '.xml':
        print '>>> saving to an non .xml file'

    return string


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('csvfile', type=_csv_file, help='a .csv file')
    parser.add_argument('-o', dest='outfile', metavar='outfile',
                        help='output file', type=_xml_file)
    args = parser.parse_args()

    # build
    tree = build_tree(args.csvfile)
    args.csvfile.close()

    # save
    outfile = (args.outfile
               if args.outfile
               else os.path.basename(args.csvfile.name)[:-4] + '.xml')
    tree.write(outfile, 'utf-8')

    # This part is completely optional. Remove it if you don't care
    # about the format of the resulting file.
    # Reopen outfile and save it with a pretty (indented) format.
    prettify_xml(outfile, INDENT, NEWLINE)


if __name__ == '__main__':
    main()
